# trevorlitsey.com

Experiments of color and shape in CSS/Javascript

### trevor litsey dot com

- [bars](http://www.trevorlitsey.com/bars)
- [big/small](http://www.trevorlitsey.com/bigsmall)
- [float](http://www.trevorlitsey.com/float)
- [grid](http://www.trevorlitsey.com/grid)
- [no fun](http://www.trevorlitsey.com/nofun)
- [shade](http://www.trevorlitsey.com/shade)
- [speck](http://www.trevorlitsey.com/speck)
- [buzz](http://www.trevorlitsey.com/buzz)
- [tri](http://www.trevorlitsey.com/tri)
- [zoom](http://www.trevorlitsey.com/zoom)
- [i hope this feeling lasts](http://www.trevorlitsey.com/i-hope-this-feeling-lasts)

### diy postmodern dot com

- [anni](http://www.diypostmodern.com/anni)
- [josef](http://www.diypostmodern.com/josef)
- [on a clear day](http://www.diypostmodern.com/on-a-clear-day)
- [r. mutt](http://www.diypostmodern.com/r-mutt)
- [sol86](http://www.diypostmodern.com/sol86)
- [sol138](http://www.diypostmodern.com/sol138)
- [sol289](http://www.diypostmodern.com/sol289)
- [sweet](http://www.diypostmodern.com/sweet)
- [this is not a sad boy](http://diypostmodern.com/thisisnotasadboy)
- [ernst](http://diypostmodern.com/ernst)

### this is not a sad boy dot com

- [a fair amount of failures](http://www.thisisnotasadboy.com/a-fair-amount-of-failures)
- [dotty](http://www.thisisnotasadboy.com/dotty)
- [talky](http://www.thisisnotasadboy.com/talky)
- [ptrk](http://www.thisisnotasadboy.com/ptrk)
- [you make me laugh but its not funny](http://www.thisisnotasadboy.com/youmakemelaughbutitsnotfunny)
- [gray](http://www.thisisnotasadboy.com/gray)

### sadness dot online

- [sadness.online](http://www.sadness.online)
